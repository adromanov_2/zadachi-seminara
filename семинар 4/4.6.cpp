#include <iostream>
#include <vector>
using namespace std;
int func(vector<int> v)
{
    double s=0;
    for(auto& x:v)
    {
        s+=x;
    }
    s/=v.size();
    return s;
}
int main()
{
    vector<int> v(100,2e8);
    double c=func(v);
    cout<<"Result:"<<c;
    return 0;
}
