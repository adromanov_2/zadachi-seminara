#include <iostream>
#include <vector>
using namespace std;
void func(vector<int>& v)
{
    for(auto x:v)
    {
        cout<<"Element:";
        cout<<x<<endl;
    }
}
int main()
{
    vector<int> v(10,5);
    func(v);
    return 0;
}
