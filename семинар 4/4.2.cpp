#include <iostream>
#include <vector>
using namespace std;
void func(vector<int>& v)
{
    int n;
    int e;
    if(!v.size())
    {
        cout<<"Input n:";
        cin>>n;
        for(int i=0;i<n;i++)
        {
            cout<<"Input element:";
            cin>>e;
            v.push_back(e);
        }
    }
    else
    {
        n=v.size();
        for(int i=0;i<n;i++)
        {
            cout<<"Input element:";
            cin>>e;
            v[i]=e;
        }
    }
}
int main()
{
    vector<int> v;
    func(v);
    for(auto x:v)
    {
        cout<<"Element:";
        cout<<x<<endl;
    }
    return 0;
}
