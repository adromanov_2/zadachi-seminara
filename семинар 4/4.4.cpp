#include <iostream>
#include <vector>
#include <exception>
#include <cstdlib>
using namespace std;
int func(vector<int> s)
{
	int n,c;
	cout << "Input n:";
	cin >> n;
	try
	{
		c= s.at(n);
	}
	catch (out_of_range e)
	{
		cout << "Bad index.";
	}
	try
	{
		if (n >= s.size())
			throw out_of_range("");
		c=s[n];
	}
	catch (out_of_range e)
	{
		cout << "Bad index.";
		exit(10);
	}
	return c;
}
int main()
{
	vector<int> s(10, 5);
	int c=func(s);
	cout << c;
	return 0;
}
