#include <iostream>
#include <vector>
#include <iterator>
using namespace std;
int func(vector<int> v)
{
    int c=v[0]*v[v.size()-1];
    return c;
}
int main()
{
    vector<int> v(10);
    int i=7;
    vector<int>::iterator it;
    for(it=v.begin();it!=v.end();it++)
    {
        *it=i;
        i++;
    }
    cout<<"Our vector:"<<endl;
    for(it=v.begin();it!=v.end();it++)
    {
        cout << *it << endl;
    }
    int c=func(v);
    cout<<"Result:"<<c;
    return 0;
}
