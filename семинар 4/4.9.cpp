#include <iostream>
#include <vector>
using namespace std;
void func(vector<int>& v)
{
    int r;
    for(unsigned int i=0;i<v.size();i++)
    {
        r=0;
        for(unsigned int j=i+1;j<v.size();j++)
        {
            if(v[i]!=v[j])
                break;
            for(unsigned int k=j;k<v.size()-1;k++)
                v[k]=v[k+1];
            v.resize(v.size()-1);
            j--;
            r=1;
        }
        if(r==1)
        {
            for(unsigned int k=i;k<v.size()-1;k++)
                v[k]=v[k+1];
            v.resize(v.size()-1);
            i--;
        }
    }
}
int main()
{
    vector<int> v(4);
    for(auto&x :v)
    {
        cin>>x;
    }
    func(v);
    cout<<"Result:"<<endl;
    for(auto&x :v)
    {
        cout<<x;
    }
    return 0;
}
