#include <iostream>
#include <vector>
using namespace std;

int main()
{
    int n,i=1;
    cout << "Input n:";
    cin>>n;
    vector<int> v(n);
    for(auto& x:v)
    {
        x=i;
        i++;
    }
    for(auto& x:v)
    {
        if((x%3==0)&&(x%5!=0))
            cout<<"three"<<endl;
        else if((x%5==0)&&(x%3!=0))
            cout<<"five"<<endl;
        else if((x%5==0)&&(x%3==0))
            cout<<"threefive"<<endl;
        else
            cout<<x<<endl;;
    }
    return 0;
}
