#include <iostream>
#include <vector>
#include <cstdlib>
using namespace std;
double func(vector<double> v)
{
    double s1=0,s2=0,s,p,k=0;
    for(auto& x:v)
    {
        cout<<"Input p:";
        cin>>p;
        k+=p;
        s1+=x*x*p;
        s2+=x*p;
    }
    s2*=s2;
    s=s1-s2;
    if(k!=1)
    {
        cout<<"Bad p.";
        exit(10);
    }
    return s;
}
int main()
{
    vector<double> v(5);
    for(auto& x:v)
    {
        cout<<"Element:";
        cin>>x;
    }
    double c=func(v);
    cout<<"Result:"<<c;
    return 0;
}
