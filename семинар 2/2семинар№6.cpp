#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    int N,M;
    int j,i,k=1;
    cout<<"N:";
    cin>>N;
    cout<<"M:";
    cin>>M;
    int** a=new int* [N];
    for(i=0;i<N;i++)
    {
        a[i]=new int[M];
    }
    for(i=0;i<N;i++)
    {
        for(j=0;j<M;j++)
        {
            if(i==j)
            {
                a[i][j]=k;
                k++;
            }
            else if(i==j-1)
                a[i][j]=2;
            else if(i==j+1)
                a[i][j]=10;
            else
                a[i][j]=0;
        }
    }
    for(i=0;i<N;i++)
    {
        for(j=0;j<M;j++)
            cout<<setw(3)<<a[i][j];
        cout<<endl;
    }
    for(i=0;i<N;i++)
        delete[]a[i];
    return 0;
}
