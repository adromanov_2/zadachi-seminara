#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	int N, M;
	int  i;
	cout << "N:";
	cin >> N;
	cout << "M:";
	cin >> M;
	int** a = new int*[N];
	int *p;
	for (i = 0; i<N; i++)
	{
		a[i] = new int[M];
	}
	i=0;
	while(1)
    {
        for (p=a[i];p<a[i]+M;p++)
            cin>>*p;
		i++;
		if(i==N)
            break;
	}
	i=0;
	while(1)
    {
        for (p=a[i];p<a[i]+M;p++)
            cout<<setw(6)<<*p;
        cout << endl;
		i++;
		if(i==N)
            break;
	}
	for (i = 0; i<N; i++)
		delete[]a[i];
	return 0;
}
