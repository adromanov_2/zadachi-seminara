#include <iostream>
#include<map>
#include<string>
#include<iterator>
#include<vector>
#include<cstdlib>
#include<cmath>
using namespace std;
void print(map<string, double> m)
{
	for (auto it = m.begin(); it != m.end(); ++it)
	{
		cout << it->first << ":" << it->second << endl;
	}
}
void Size(map<string, double> m)
{
	cout << m.size() << endl;
}
void add(map<string, double>& m, string s, double x)
{
	if (m.count(s))
		m[s] += x;
	else
		m.insert(pair<string, double>(s, x));
}
void task(vector<string> v, map<string, double> m)
{
	for (unsigned int i = 0; i<v.size(); i++)
	{
		string s;
		double x = 0;
		if (v[i] == "SIZE")
			Size(m);
		else if (v[i] == "PRINT")
			print(m);
		else if (v[i][0] == 'A'&&v[i][1] == 'D'&&v[i][2] == 'D')
		{
			v[i].push_back(' ');
			int j = 3, u;
			string q, w;
			while (1)
			{
				if (v[i][j] != ' '&&v[i][j] != '\n')
					break;
				j++;
			}
			while (1)
			{
				s.push_back(v[i][j]);
				j++;
				if (v[i][j] == ' ' || v[i][j] == '\n')
					break;
			}
			while (1)
			{
				if (v[i][j] != ' '&&v[i][j] != '\n')
					break;
				j++;
			}
			if (v[i][j]<'0' || v[i][j]>'9')
			{
				cout << "Incorrect." << endl;
				system("pause");
				exit(10);
			}
			while (1)
			{
				if (v[i][j]<'0' || v[i][j]>'9')
					break;
				q.push_back(v[i][j]);
				j++;
			}
			u = stoi(q);
			x += u;
			if (v[i][j] == '.')
			{
				while (1)
				{
					j++;
					if (v[i][j]<'0' || v[i][j]>'9')
						break;
					w.push_back(v[i][j]);
				}
				u = stoi(w);
				x += (u / pow(10, w.size()));
			}
			add(m,s,x);
		}
		else
		{
			cout << "Incorrect command." << endl;
			system("pause");
			exit(10);
		}
	}

}
int main()
{
	string s;
	map<string, double> m;
	vector<string> v;
	cout<<"Example of input."<<endl;
	cout<<"ADD as 12"<<endl<<"ADD sd 123.23"<<endl<<"PRINT"<<endl<<"SIZE"<<endl;
	cout<<"To end input click on enter twice."<<endl;
	while (1)
	{
		getline(cin, s, '\n');
		if (s.size() == 0)
			break;
		v.push_back(s);
	}
	task(v, m);
	system("pause");
	return 0;
}
