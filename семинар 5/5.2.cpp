#include <iostream>
#include <random>
#include <algorithm>
using namespace std;
int main()
{
    mt19937 gen;
    int L,H=0;
    uniform_int_distribution<int> uid(0,1);
    cout<<"Input L:";
    while(1)
    {
        cin>>L;
        if(L<=0)
            cout<<"Input again."<<endl;
        else
            break;
    }
    vector<int> v(L);
    generate(v.begin(),v.end(),[&uid,&gen]()->int{return uid(gen);});
    for(unsigned int i=0;i<v.size();i++)
    {
        if(v[i]==0)
            v[i]=-1;
    }
    for(unsigned int i=0;i<v.size();i++)
    {
        cout<<v[i]<<" ";
    }
    cout<<endl;
    for(int i=0;i<L-1;i++)
    {
        H+=v[i]*v[i+1];
    }
    H+=v[L-1]*v[0];
    cout<<"Hamiltonean:"<<H<<endl;
    int r;
    cout<<"Input numer to change spin(from 1 to L):";
    while(1)
    {
        cin>>r;
        if(r<=0||r>L)
            cout<<"Input again."<<endl;
        else
            break;
    }
    v[r-1]=-v[r-1];
    H=0;
    for(int i=0;i<L-1;i++)
    {
        H+=v[i]*v[i+1];
    }
    H+=v[L-1]*v[0];
    cout<<"Hamiltonean2:"<<H<<endl;
    return 0;
}
