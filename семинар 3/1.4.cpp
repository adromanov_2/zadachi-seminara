#include <iostream>
#include <exception>
#include <cmath>
using namespace std;
double mysqrt(double a)
{
	double c;
	if (a < 0)
		throw logic_error("cant be <0.");
	else
		return c = sqrt(a);
}
int main()
{
	double a;
	cout << "Input a:" << endl;
	cin >> a;
	double c;
	try
	{
		c = mysqrt(a);
	}
	catch (logic_error e)
	{
		cout << e.what();
		return 0;
	}
	if(a>=0)
		cout << c;
	return 0;
}
