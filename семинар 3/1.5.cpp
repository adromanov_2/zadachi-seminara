#include <iostream>
#include <exception>
#include <cmath>
using namespace std;
void myf()
{
	throw logic_error("Error.");
}
void b()
{
	myf();
}
int main()
{
	try
	{
		b();
	}
	catch (logic_error e)
	{
		cout << e.what();
	}
	return 0;
}
