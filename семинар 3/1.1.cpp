#include <iostream>
#include <exception>
using namespace std;

int main()
{
	int a;
	cout << "Inter age:" << endl;
	cin >> a;
	try
	{
		if (a<0)
			throw logic_error("Age cannot be negative.");
	}
	catch (logic_error e)
	{
		cout << e.what();
	}
	return 0;
}
