#include <iostream>
#include <string>
#include <exception>
using namespace std;

int main()
{
	string s;
	int n, m;
	cout << "Input string:";
	cin >> s;
	cout << "Input n:";
	cin >> n;
	cout << "Input m:";
	cin >> m;
	cout << "For at():";
	try
	{
		cout << s.at(n) << endl;
	}
	catch (out_of_range e)
	{
		cout << "Bad index.";
		return 0;
	}
	cout << "For []:";
	try
	{
		if(m>=s.size())
			throw out_of_range("");
		cout<<s[m];
	}
	catch (out_of_range e)
	{
		cout << "Bad index.";
		return 0;
	}
	return 0;
}
