#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main()
{
    string s;
    cout<<"Input string:";
    cin>>s;
    cout <<"First symbol:"<<*(s.begin())<<endl;
    cout <<"Last symbol:"<<*(s.end()-1);
    return 0;
}
