#include <iostream>
#include <string>
#include <iterator>
using namespace std;

int main()
{
	string s;
	cin >> s;
	int j = 0;
	string::iterator it, it2,b;
	for (it = s.begin(); it != s.end(); it++)
	{
		if (*it == '*')
			j++;
	}
	try
	{
		if (j % 2 != 0)
			throw logic_error("You entered bad string");
	}
	catch (logic_error t)
	{
		cout << t.what();
		return 0;
	}
	for (it = s.begin(); it != s.end();)
	{
		if (*it == '*')
		{
			for (it2 = it + 1; it2 != s.end(); it2++)
			{
				if (*it2 == '*')
					break;
			}
			it = s.erase(it, it2 + 1);
		}
		else
			it++;
	}
	cout << s;
	return 0;
}
