#include <iostream>
#include <regex>
#include <string>
#include <exception>
using namespace std;

int main()
{
	string s, p;
	smatch m;
	int i = 0;
	getline(cin, s);
	regex e("[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}");
	bool b = regex_search(s, e);
	try
	{
		if (!b)
		{
			throw logic_error("Bad format.\n");
		}
		regex g("[0-9]{4}");
		while (regex_search(s, m, g))
		{
			for (auto x : m)
			{
				if (i == 0)
				{
					if ((x > "2018") || (x < "0001"))
						throw logic_error("Bad format.\n");
				}


				if (i == 1)
				{
					p = x;
					if ((x > "12") || (x < "01"))
						throw logic_error("Bad format.\n");
				}
				if (i == 2)
				{
					if ((x < "01") || (x > "31"))
						throw logic_error("Bad format.\n");
					if ((p == "02") && (x > "28"))
						throw logic_error("Bad format.\n");
					if (((p == "04") || (p == "06") || (p == "09") || (p == "11")) && (x > "30"))
						throw logic_error("Bad format.\n");
				}
				if (i == 3)
					if (x > "23")
						throw logic_error("Bad format.\n");
				if (i == 4)
					if (x > "59")
						throw logic_error("Bad format.\n");
				if (i == 5)
					if (x > "59")
						throw logic_error("Bad format.\n");
			}
			g = "[0-9]{2}";
			s = m.suffix().str();
			i++;
		}
	}
	catch (logic_error e)
	{
		cout << e.what();
		return 0;
	}
	cout << "Good format.";
	return 0;
}
