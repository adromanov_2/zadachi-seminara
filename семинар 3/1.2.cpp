#include <iostream>
#include <exception>
using namespace std;

int main()
{
	double a;
	cout << "Input a:" << endl;
	cin >> a;
	double b;
	double c;
	cout << "Input b:" << endl;
	cin >> b;
	try
	{
		if (b == 0)
			throw logic_error("B cannot be 0.");
	}
	catch (logic_error e)
	{
		cout << e.what();
		return 0;
	}
	c = a / b;
	cout << c;
	return 0;
}

