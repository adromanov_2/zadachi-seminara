#include <iostream>
#include <exception>
#include <cmath>
using namespace std;

int main()
{
	double a;
	cout << "Inter a:" << endl;
	cin >> a;
	double c;
	try
	{
		if (a<=0)
			throw logic_error("cant be <=0.");
	}
	catch (logic_error e)
	{
		cout << e.what();
		return 0;
	}
	c = log(a);
	cout << c;
	return 0;
}
